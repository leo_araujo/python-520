#!/usr/bin/python3
from psycopg2 import connect
from faker import Faker

fake = Faker('pt-BR')  

try:
    con = connect('host=127.0.0.1 user=admin password=4linux dbname=projeto')
    cur = con.cursor()

    # cur.execute("select * from usuarios")
    # print(cur.fetchall())

    # n1 = fake.name()
    # e1 = fake.email()
    # c1 = fake.cpf()
    # s1 = fake.sha256()
    # print()

    for i in range(1000):
        cur.execute("insert into usuarios (nome, email, cpf, senha) "
            "values ('{}','{}','{}','{}')".format(
            fake.name(), fake.email(), fake.cpf(), fake.sha256()
        ))

except Exception as e:
    con.rollback()
    print(e)

finally:
    if 'cur' in globals():
        cur.close()

    if 'con' in globals():
        con.commit()
        con.close()    

# sql = 'select * from usuarios'
# cur.execute(sql)
# sql = "insert into cidade values (default,'São Paulo','SP')"
# cur.execute(sql)
# con.commit()
# cur.execute('select * from cidade')
# recset = cur.fetchall()

# for rec in recset:
#     print (rec)

# con.close()