#!/usr/bin/python3
'''
criar uma classe conta  
atributos: titular, saldo
metodos: sacar, depositar, transferir
'''

class Conta():
    def __init__(self, titular, saldo = 0):
        self.titular = titular
        self.saldo = saldo

    def sacar(self, valor):
        if self.saldo >= valor:
            self.saldo -= valor
            print('Saque titular: {}, valor: {}'.format(
                self.titular, self.saldo))
            return True
        else:
            print('Saldo insuficiente titular: {}, Saldo: {}'.format(self.titular, self.saldo))
            return False

    def depositar(self, valor):
        self.saldo += valor
        print('Valor depositado com sucesso! ', self.saldo)

    def transferir(self, valor, Conta):
        try:
            if not self.sacar(valor):
                raise ValueError('Falha ao realizar transferência!')
            try:
                Conta.depositar(valor)
            except Exception:
                self.depositar(valor)
                raise ValueError('Falha ao realizar transferência!')
            return True
        except ValueError as e:
            print(e)
        print('Saldo Atual: {}'.format(self.saldo))


    # def __str__(self):
    #     return 'titular: {} - Saldo: {}'.format(self.titular, self.saldo)


class Poupanca(Conta):
    def __init__(self, titular, saldo=0):
        super().__init__(titular, saldo)
        self.juros = 0.005

    def render_juros(self):
        self.saldo += self.saldo * self.juros
