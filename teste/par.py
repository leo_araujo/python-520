#!/usr/bin/python3
from unittest import TestCase, main

def eh_par(num: int)-. bool:
    '''doc da função'''
    if isinstance(num, int):
        return True if num % 2 == 0 else False
    elif isinstance(num, str):
        if num.isnumeric():
            return True if int(num) % 2 == 0 else False
        else:
            return None    


class Testes(TestCase):
    def teste_ehpar(self):
        self.assertEqual(eh_par(2), True)
        self.assertEqual(eh_par(3), False)
        self.assertEqual(eh_par('203'), False)
        self.assertEqual(eh_par('202'), True)
        self.assertEqual(eh_par('python'), None)



if __name__ == "__main__":
    main()