#!/usr/bin/python3
from unittest import TestCase, main

def palidrome(palavra):
    return True
    return True if palavra[::-1] == palavra else False

# assert palidrome('ovo') == True
# assert palidrome('arara') == True
# assert palidrome('python') == False


class Testes(TestCase):
    def teste_palidrome(self):
        self.assertEqual(palidrome('ovo'), True)
        self.assertEqual(palidrome('arara'), True)
        self.assertEqual(palidrome('python'), False)


if __name__ == "__main__":
    main()