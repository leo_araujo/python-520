#!/usr/bin/python3

ling = input('Qual a melhor linguagem? ')
ling = ling.strip().lower()

try:
    if ling == 'python':
        print('acertou')
    elif ling == 'java' or ling == 'golang':
        print('quase')
    elif ling == 'html' or ling == 'css':
        raise ValueError('isso não é linguagem de programação!')
    else:
        print('nunca ouvi falar')    
except ValueError as e:
    print(e)

