#!/usr/bin/python3

'''
função anonima
'''

a = lambda x : x + 2
print(a(3),'\n')


anon  = [lambda x : x + 2,
lambda x : x + 4,
lambda x : x + 6
]
for x in anon:
    print(x(3),'\n')


print(list(map(lambda x : x **2, range(11) )))


num = [1,35,35,8,7,6,8,4,35,1,9,10]
print(set(num))

num.append(0)
print(all(num))