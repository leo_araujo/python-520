#!/usr/bin/python3

# arquivo = open('teste.txt', 'r')
# print(arquivo.read())

# arquivo = open('teste.txt', 'r')

# print(arquivo.readline())

# print(arquivo.readline())

# print(arquivo.seek(0))

# print(arquivo.readline())

# print(arquivo.readlines())

# arquivo.close()

# with open('teste.txt', 'r') as arquivo:
#     conteudo = arquivo.readlines()

# with open('teste.txt', 'a') as arquivo:
#     arquivo.write('novalinha\n')

# conteudo = 10*['teste de nova linha\n']
# with open('teste.txt', 'w') as arquivo:
#     arquivo.writelines(conteudo)

# print(conteudo)    

nomearquivo = input('Digite um nome para o novo arquivo: ').strip() + '.txt'
shebang = '#!/usr/bin/python3\n'

try:
    with open(nomearquivo, 'x') as arquivo:
        arquivo.write(shebang)
except FileExistsError: 
    with open(nomearquivo, 'r') as arquivo:
        if arquivo.readline() != shebang:
            arquivo.seek(0)
            conteudo = arquivo.readlines()
            conteudo.insert(0, shebang)
            with open(nomearquivo, 'w') as arquivo:
                arquivo.writelines(conteudo)


print('terminou ', nomearquivo) 