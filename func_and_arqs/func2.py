#!/usr/bin/python3
'''
ler_arquivo
escrever_arquivo
contar_linhas
'''
def ler_arquivo(arquivo):
    try:
        with open(arquivo, 'r') as arq:
            return arq.readlines()
    except FileNotFoundError:
        return[]        

def escrever_arquivo(arquivo, conteudo):
    with open(arquivo, 'a') as arq:            
        arq.write(conteudo)

def contar_linhas(arquivo):
        return len(ler_arquivo(arquivo))

nomearquivo = input('informe o nome do arquivo: ')

texto = ler_arquivo(nomearquivo).__doc__

escrever_arquivo(conteudo='texto',arquivo='nomearquivo')

linhas = contar_linhas(nomearquivo)
        
print(linhas)