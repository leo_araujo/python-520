#!/usr/bin/python3
from flask import Flask, jsonify, render_template
from pymongo import MongoClient
from psycopg2 import connect


try:
    con_2= connect(
        'host=127.0.0.1 user=admin password=4linux dbname=projeto'
    )
    cur = con_2.cursor()
    con = MongoClient()
    db = con['projeto']

except Exception as e:
    print(e)
    exit() 

app = Flask(__name__) 

@app.route('/')
def index():
    # print(list(db.usuarios.find()))
    mensagem = {'Mensagem': "minha aprimeira api rest"}

    return jsonify(mensagem)

@app.route('/usuario')   
def busca_usuario():
    usuarios = list(db.usuarios.find())
    for registro in usuarios:
        registro['_id'] = str(registro['_id'])
        print(type(registro['_id']))

    return jsonify(usuarios)

@app.route('/usuario/<string:busca>')   
def usuarios_api(busca):
    usuario = db.usuarios.find_one({'nome':busca})
    usuario['_id'] = str(usuario['_id'])
    # print(db.usuarios.find_one({'email':busca}))
    return jsonify(usuario)

@app.route('/web')   
def web_render():
    cur.execute("select * from usuarios")
    return render_template("index.html", usuarios=cur.fetchall())
# index tem que ficar na pasta templates

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5000)